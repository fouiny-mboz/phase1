package bdd.airport;

import org.junit.jupiter.api.*;


import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


class AirportTest {

    @DisplayName("Given there is an economy flight")
    @Nested
    class EconomyFlightTest {
       private Flight economyFlight;
       private Passenger mike;
       private Passenger james;

       @BeforeEach
       void setUp() {
           economyFlight = new EconomyFlight("1");
           mike = new Passenger("Mike", false);
           james = new Passenger("James", true);
       }

       @Nested
       @DisplayName("When we have a regular passenger")
       class RegularPassenger {
           @Test
           @DisplayName("Then you can add and remove him from economy flight")
           public void testEconomyFlightRegularPassenger() {
               assertAll("Verify all conditions for all a regular passenger and economy flight",
                       () -> assertEquals("1", economyFlight.getId()),
                       () -> assertEquals(true, economyFlight.addPassenger(mike)),
                       () -> assertEquals(1, economyFlight.getPassengersSet().size()),
                       () -> assertEquals("Mike", new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName()),
                       () -> assertEquals(true, economyFlight.removePassenger(mike)),
                       () -> assertEquals(0, economyFlight.getPassengersSet().size())
               );
           }

           @DisplayName("Then you cannot add him to an econmy flight more than once")
           @RepeatedTest(5)
           public void testEconomyFlightRegularPassengerOnlyOnce(RepetitionInfo repetitionInfo) {
               for (int i = 0; i < repetitionInfo.getCurrentRepetition(); ++i) {
                   economyFlight.addPassenger(mike);
               }
               assertAll("Verify a regular passenger can be added to an economy flight only once",
                       () -> assertEquals(1, economyFlight.getPassengersSet().size()),
                       () -> assertTrue(economyFlight.getPassengersSet().contains(mike)),
                       () -> assertEquals("Mike", new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName())
               );
           }
       }

       @Nested
       @DisplayName("When we have vip passenger")
       class VipPassenger {

           @DisplayName("Then you cannot add him to an economy flight more than once")
           @RepeatedTest(5)
           public void testEconomyFlightPassengerAddedOnlyOnce(RepetitionInfo repetitionInfo) {
               for (int i = 0; i < repetitionInfo.getCurrentRepetition(); i++) {
                   economyFlight.addPassenger(james);
               }
               assertAll("Verify a VIP passenger can be added to an economy flight only once",
                       () -> assertEquals(1, economyFlight.getPassengersSet().size()),
                       () -> assertTrue(economyFlight.getPassengersSet().contains(james)),
                       () -> assertTrue(new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName().equals("James"))
               );
           }

       }
   }

   @DisplayName("Given there is a busness flight")
   @Nested
   class BusinessFlightTest {
       private Flight businessFlight;
       private Passenger mike;
       private Passenger james;

       @BeforeEach
       void setUp(){
           businessFlight = new BusinessFlight("2");
           mike = new Passenger("Mike", false);
           james = new Passenger("James", true);
       }

       @Nested
       @DisplayName("When we have a regular passenger")
       class RegularPassenger{

           @Test
           @DisplayName("Then you cannot add and remove him from business flight")
           public void testBusinessFlightPassenger(){
               assertAll("Verify all conditions for a regular passenger and business flight",
                       () -> assertEquals(false, businessFlight.addPassenger(mike)),
                       () -> assertEquals(0, businessFlight.getPassengersSet().size()),
                       () -> assertEquals(false, businessFlight.removePassenger(mike)),
                       () -> assertEquals(0, businessFlight.getPassengersSet().size())
               );
           }
       }

       @Nested
       @DisplayName("When we have a VIP passenger")
       class VipPassenger {

           @Test
           @DisplayName("Then you can add him but cannot remove him a business flight")
           public void testBusinessFlightVipPassenger(){
               assertAll("Verify all conditions for a VIP passenger and business flight",
                       ()   -> assertEquals(true, businessFlight.addPassenger(james)),
                       ()   -> assertEquals(1, businessFlight.getPassengersSet().size()),
                       ()   -> assertEquals(false, businessFlight.removePassenger(james)),
                       ()   -> assertEquals(1, businessFlight.getPassengersSet().size())
               );
               }
           }
       }

       @DisplayName("Given there is a premium flight")
       @Nested
       class PremiumFlightTest{
            private Flight premiumFight;
            private Passenger mike;
            private Passenger james;

            @BeforeEach
            void setUp(){

                premiumFight = new PremiumFlight("3");
                mike = new Passenger("Mike", false);
                james = new Passenger("James", true);
            }

            @Nested
            @DisplayName("When we have a regular passenger")
            class RegularPassenger{

                @Test
                @DisplayName("Then you cannot add and remove him an premium flight")
                public void testPremiumFlightRegularPassenger(){
                    assertAll("Verify all conditions for regular passenger and premium class ",
                            ()  -> assertEquals(false, premiumFight.addPassenger(mike)),
                            ()  -> assertEquals(0, premiumFight.getPassengersSet().size()),
                            ()  -> assertEquals(false, premiumFight.removePassenger(mike)),
                            ()  -> assertEquals(0, premiumFight.getPassengersSet().size())
                    );
                }
            }

            @Nested
            @DisplayName("When you have a vip passenger")
            class VipPassenger{

                @Test
                @DisplayName("Then you can add and remove him an premium flight")
                public void testPremiumFlightVipPassenger(){
                    assertAll("Verify all conditions for vip passenger and premium flight",
                            ()  -> assertEquals(true, premiumFight.addPassenger(james)),
                            ()  -> assertEquals(1, premiumFight.getPassengersSet().size()),
                            ()  -> assertTrue(premiumFight.getPassengersSet().contains(james)),
                            ()  -> assertEquals("James", new ArrayList<>(premiumFight.getPassengersSet()).get(0).getName()),
                            () -> assertEquals(true, premiumFight.removePassenger(james)),
                            ()  -> assertEquals(0, premiumFight.getPassengersSet().size())
                    );
                }
            }
       }


}
